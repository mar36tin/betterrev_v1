# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table contribution (
  id                        bigint auto_increment not null,
  repository_id             varchar(255) not null,
  pull_request_id           varchar(255) not null,
  name                      varchar(255) not null,
  description               varchar(255),
  state                     varchar(23) not null,
  requester_id              bigint,
  created_on                timestamp not null,
  updated_on                timestamp not null,
  branch_name               varchar(255) not null,
  constraint ck_contribution_state check (state in ('NULL','OPEN','PENDING_PEER_REVIEW','PENDING_MENTOR_APPROVAL','ACCEPTED','CLOSED','COMMITTED')),
  constraint pk_contribution primary key (id))
;

create table contribution_event (
  id                        bigint auto_increment not null,
  contribution_id           bigint,
  contribution_event_type   varchar(27),
  link_to_external_info     varchar(255),
  created_date              timestamp,
  constraint ck_contribution_event_contribution_event_type check (contribution_event_type in ('CONTRIBUTION_GENERATED','CONTRIBUTION_MODIFIED','MENTOR_NOTIFICATION_REQUEST','MENTOR_NOTIFIED','WEBREV_GENERATED','MERGED','APPROVED','REJECTED','TERMINATED','PEER_REVIEWED')),
  constraint pk_contribution_event primary key (id))
;

create table interest (
  id                        bigint auto_increment not null,
  path                      varchar(255),
  project                   varchar(255),
  constraint pk_interest primary key (id))
;

create table mentor (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  email                     varchar(255),
  mentor_type               varchar(10),
  created_date              timestamp,
  constraint ck_mentor_mentor_type check (mentor_type in ('INDIVIDUAL','PROJECT','LIST')),
  constraint pk_mentor primary key (id))
;

create table tag (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  constraint pk_tag primary key (id))
;

create table betterrev_user (
  id                        bigint auto_increment not null,
  bitbucket_user_name       varchar(255),
  openjdk_username          varchar(255),
  name                      varchar(255),
  oca_status                varchar(10),
  created_date              timestamp,
  constraint ck_betterrev_user_oca_status check (oca_status in ('SIGNED','NOT_SIGNED','UNKNOWN')),
  constraint pk_betterrev_user primary key (id))
;


create table contribution_tag (
  contribution_id                bigint not null,
  tag_id                         bigint not null,
  constraint pk_contribution_tag primary key (contribution_id, tag_id))
;

create table contribution_mentor (
  contribution_id                bigint not null,
  mentor_id                      bigint not null,
  constraint pk_contribution_mentor primary key (contribution_id, mentor_id))
;

create table mentor_interest (
  mentor_id                      bigint not null,
  interest_id                    bigint not null,
  constraint pk_mentor_interest primary key (mentor_id, interest_id))
;
alter table contribution add constraint fk_contribution_requester_1 foreign key (requester_id) references betterrev_user (id) on delete restrict on update restrict;
create index ix_contribution_requester_1 on contribution (requester_id);
alter table contribution_event add constraint fk_contribution_event_contribu_2 foreign key (contribution_id) references contribution (id) on delete restrict on update restrict;
create index ix_contribution_event_contribu_2 on contribution_event (contribution_id);



alter table contribution_tag add constraint fk_contribution_tag_contribut_01 foreign key (contribution_id) references contribution (id) on delete restrict on update restrict;

alter table contribution_tag add constraint fk_contribution_tag_tag_02 foreign key (tag_id) references tag (id) on delete restrict on update restrict;

alter table contribution_mentor add constraint fk_contribution_mentor_contri_01 foreign key (contribution_id) references contribution (id) on delete restrict on update restrict;

alter table contribution_mentor add constraint fk_contribution_mentor_mentor_02 foreign key (mentor_id) references mentor (id) on delete restrict on update restrict;

alter table mentor_interest add constraint fk_mentor_interest_mentor_01 foreign key (mentor_id) references mentor (id) on delete restrict on update restrict;

alter table mentor_interest add constraint fk_mentor_interest_interest_02 foreign key (interest_id) references interest (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists contribution;

drop table if exists contribution_tag;

drop table if exists contribution_mentor;

drop table if exists contribution_event;

drop table if exists interest;

drop table if exists mentor;

drop table if exists mentor_interest;

drop table if exists tag;

drop table if exists betterrev_user;

SET REFERENTIAL_INTEGRITY TRUE;

