package models;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.utils.dao.BasicModel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Mentor entity class representing a conceptual thing that can sponsor a
 * Contribution, for example, a person, project or mailing list.
 */
@Entity
public class Mentor extends Model implements BasicModel<Long> {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, Mentor> find = new Model.Finder<>(Long.class, Mentor.class);

    public enum MentorType {
        INDIVIDUAL, PROJECT, LIST
    }

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @Required
    public String name;

    @Email
    @Required
    public String email;

    @Required
    @Enumerated(EnumType.STRING)
    public MentorType mentorType;

    // TODO -
    // https://bitbucket.org/adoptopenjdk/betterrev/issue/5/decide-if-we-should-refine-mentorinterests
    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Interest> interests = new HashSet<>();

    public Date createdDate;

    public Mentor(String name, String email, MentorType mentorType) {
        this.name = name;
        this.email = email;
        this.mentorType = mentorType;
        this.createdDate = new Date();
    }

    public static List<Mentor> findRelevantMentors(String repository, Set<String> paths) {
        List<Mentor> relevantMentors = new ArrayList<>();
        List<Mentor> allMentors = find.all();
        for (Mentor mentor : allMentors) {
            if (mentor.caresAbout(repository, paths)) {
                relevantMentors.add(mentor);
            }
        }
        return relevantMentors;
    }

    private boolean caresAbout(String repository, Set<String> paths) {
        for (Interest interest : interests) {
            if (interest.caresAbout(repository, paths)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Mentor [id=" + id + ", name=" + name + ", email=" + email + ", mentorType=" + mentorType
                + ", interests=" + interests + ", createdDate=" + createdDate + "]";
    }

    @Override
    public Long getKey() {
        return id;
    }

    @Override
    public void setKey(Long key) {
        id = key;
    }
}
