package models;

public enum ContributionEventType {
    /**
     * A user has created a Pull Request on Bitbucket,
     * which has been triggered a Contribution to be generated
     */
    CONTRIBUTION_GENERATED(true),

    /**
     * A user has created a modified their Pull Request on Bitbucket,
     * which has triggered the corresponding Contribution to be modified
     */
    CONTRIBUTION_MODIFIED(true),

    /**
     * We've notified the mentor that they need to review the Contribution
     */
    MENTOR_NOTIFICATION_REQUEST(false),

    /**
     * We've notified the mentor that they need to review the Contribution
     */
    MENTOR_NOTIFIED(true),

    /**
     * we've generated a webrev and put it up online
     */
    WEBREV_GENERATED(true),

    /**
     * Upstream has merged this Contribution into our master
     */
    MERGED(true),

    /**
     * Upstream has merged this Contribution into their codebase
     */
    APPROVED(true),

    /**
     * This Contribution has been rejected
     */
    REJECTED(true),

    /**
     * Contribution was closed in Bitbucket without going into OpenJDK
     */
    TERMINATED(true),

    PEER_REVIEWED(true);

    private final boolean visible;

    private ContributionEventType(boolean visible) {
        this.visible = visible;
    }

    /**
     * @return whether this event is visible to the end user or not.
     */
    public boolean isVisible() {
        return visible;
    }

}