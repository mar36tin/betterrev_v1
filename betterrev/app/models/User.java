package models;

import com.google.common.base.Objects;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.utils.dao.BasicModel;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * User entity class, which represents all Users within the BetterRev
 * application.
 */
@Entity
@Table(name = "betterrev_user")
public class User extends Model implements BasicModel<Long> {

    private static final long serialVersionUID = 188525469548289315L;

    public enum OcaStatus {
        SIGNED, 
        NOT_SIGNED, 
        UNKNOWN
    }

    public static Model.Finder<Long, User> find = new Model.Finder<>(Long.class, User.class);

    public static User findOrCreate(String bitbucketUserName, String displayName) {
        User user = find.where().eq("bitbucketUserName", bitbucketUserName).findUnique();
        if (user == null) {
            user = new User(bitbucketUserName, displayName, OcaStatus.UNKNOWN);
            user.save();
        }
        return user;
    }

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @Required
    public String bitbucketUserName;

    public String openjdkUsername;

    @Required
    public String name;

    @Enumerated(EnumType.STRING)
    public OcaStatus ocaStatus;

    public Date createdDate;

    public User(String bitbucketUserName, String name, OcaStatus ocaStatus) {
        this.bitbucketUserName = bitbucketUserName;
        this.name = name;
        this.ocaStatus = ocaStatus;
        this.createdDate = new Date();
    }

    @Override
    public int hashCode() {
        return bitbucketUserName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        User other = (User) obj;
        return Objects.equal(bitbucketUserName, other.bitbucketUserName);
    }

    @Override
    public Long getKey() {
        return id;
    }

    @Override
    public void setKey(Long key) {
        id = key;
    }
}
