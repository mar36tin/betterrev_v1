package update.external;

import models.Contribution;
import models.ContributionEvent;
import models.ContributionEventType;
import models.State;

/**
 * Service Class responsible for modifying a Contribution (and updating the lifecycle events) as specified by
 * actions occurring external to the DVCS polling.<br/>
 * <br/>
 * Developers Note: The following methods are effectively stubs which can be augmented with functionality
 * related to the underlying task.
 */
public final class ContributionModifier {

    private ContributionModifier() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static Contribution notifyMentor(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, State.PENDING_MENTOR_APPROVAL);

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.MENTOR_NOTIFIED, linkToExternalInfo));
        contribution.state = State.PENDING_MENTOR_APPROVAL;
        contribution.update();

        return contribution;
    }


    public static Contribution peerReviewComplete(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, State.PENDING_PEER_REVIEW);

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.PEER_REVIEWED, linkToExternalInfo));
        contribution.state = State.PENDING_MENTOR_APPROVAL;
        contribution.update();

        return contribution;
    }

    public static Contribution terminateContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, State.OPEN);

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.TERMINATED, linkToExternalInfo));
        contribution.state = State.CLOSED;
        contribution.update();

        return contribution;
    }

    public static Contribution mentorRejectContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, State.PENDING_MENTOR_APPROVAL);

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.REJECTED, linkToExternalInfo));
        contribution.state = State.CLOSED;
        contribution.update();

        return contribution;
    }

    public static Contribution mentorApproveContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, State.PENDING_MENTOR_APPROVAL);

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.APPROVED, linkToExternalInfo));
        contribution.state = State.ACCEPTED;
        contribution.update();

        return contribution;
    }

    public static Contribution mergeContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, State.ACCEPTED);

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.MERGED, linkToExternalInfo));
        contribution.state = State.COMMITTED;
        contribution.update();

        return contribution;
    }

    private static void ensureExpectedStartState(Contribution contribution, State expectedState) {
        if (contribution.state != expectedState) {
            throw new IllegalStateException("Cannot transition from current State of " + contribution.state);
        }
    }

}
