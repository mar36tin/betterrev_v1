package utils;

import play.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public final class Processes {

    private static final String SHELL = "/bin/sh";

    private Processes() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static int runProcess(String workingDirectory, String... command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.directory(new File(workingDirectory));

        Process process = builder.start();

        try {
            int exitCode = process.waitFor();

            if (exitCode != 0) {
                Logger.error("Unknown exit code when running " + Arrays.toString(command) + " value is " + exitCode);
            }

            return exitCode;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static int runThroughShell(String workingDirectory, String... args) throws IOException {
        String[] command = new String[args.length + 1];
        command[0] = SHELL;
        System.arraycopy(args, 0, command, 1, args.length);
        return runProcess(workingDirectory, command);
    }

}
