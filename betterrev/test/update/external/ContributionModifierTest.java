package update.external;

import models.AbstractPersistenceIntegrationTest;
import models.Contribution;
import models.ContributionEvent;
import models.ContributionTest;
import models.State;
import org.junit.Before;
import org.junit.Test;

import static models.ContributionEventType.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static update.external.ContributionModifier.peerReviewComplete;
import static update.external.ContributionModifier.mentorRejectContribution;
import static update.external.ContributionModifier.mergeContribution;
import static update.external.ContributionModifier.notifyMentor;

public class ContributionModifierTest extends AbstractPersistenceIntegrationTest {

    public static final String TEST_LINK_TO_EXTERNAL_INFO = "linkToExternalInfo";

    @Before
    public void setUp() {
        for (Contribution contribution : Contribution.find.all()) {
            contribution.delete();
        }
    }

    @Test
    public void givenContributionInStatePendingMentorApproval_whenRejectedByMentor_shouldHaveLinkToExternalInfo() {
        Contribution contribution = ContributionTest.createTestInstance(State.PENDING_MENTOR_APPROVAL);
        contribution = mentorRejectContribution(contribution.repositoryId, contribution.pullRequestId, TEST_LINK_TO_EXTERNAL_INFO);

        String linkToExternalInfo = null;
        for (ContributionEvent contributionEvent : contribution.contributionEvents) {
            if (contributionEvent.contributionEventType != null && contributionEvent.contributionEventType.equals(REJECTED)) {
                linkToExternalInfo = contributionEvent.linkToExternalInfo;
                break;
            }
        }
        assertThat("Link to External Info is not set correctly", linkToExternalInfo, is(TEST_LINK_TO_EXTERNAL_INFO));
    }

    @Test
    public void givenContributionInStatePendingMentorApproval_whenRejectedByMentor_shouldContainRejectedEvent() {
        Contribution contribution = ContributionTest.createTestInstance(State.PENDING_MENTOR_APPROVAL);
        contribution = mentorRejectContribution(contribution.repositoryId, contribution.pullRequestId, TEST_LINK_TO_EXTERNAL_INFO);
        assertTrue("Could not find Rejected ContributionEventType", contribution.hasContributionEventWith(REJECTED));
    }


    @Test
    public void givenContributionInStateAccepted_whenMerged_shouldContainMergedEvent() {
        Contribution contribution = ContributionTest.createTestInstance(State.ACCEPTED);
        contribution = mergeContribution(contribution.repositoryId, contribution.pullRequestId, TEST_LINK_TO_EXTERNAL_INFO);
        assertTrue("Could not find Merged ContributionEventType", contribution.hasContributionEventWith(MERGED));
    }

    @Test
    public void givenContributionInPendingPeerReviewState_whenPeerReviewed_shouldContainPeerReviewedEvent() {
        Contribution contribution = ContributionTest.createTestInstance(State.PENDING_PEER_REVIEW);
        contribution = peerReviewComplete(contribution.repositoryId, contribution.pullRequestId, TEST_LINK_TO_EXTERNAL_INFO);
        assertTrue("Could not find ContributionEventType", contribution.hasContributionEventWith(PEER_REVIEWED));
    }

    @Test
    public void givenContributionInPendingMentorApprovalState_whenNotifyMentor_shouldContainMentorNotifiedEvent() {
        Contribution contribution = ContributionTest.createTestInstance(State.PENDING_MENTOR_APPROVAL);
        contribution = notifyMentor(contribution.repositoryId, contribution.pullRequestId, TEST_LINK_TO_EXTERNAL_INFO);
        assertTrue("Could not find ContributionEventType", contribution.hasContributionEventWith(MENTOR_NOTIFIED));
    }


    @Test(expected = IllegalStateException.class)
    public void givenContributionInOpenState_shouldPreventMentorNotification() {
        Contribution contribution = ContributionTest.createTestInstance(State.OPEN);
        notifyMentor(contribution.repositoryId, contribution.pullRequestId, TEST_LINK_TO_EXTERNAL_INFO);
    }

}
