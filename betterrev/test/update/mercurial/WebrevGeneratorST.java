package update.mercurial;

import akka.actor.ActorRef;
import akka.event.EventStream;
import models.AbstractPersistenceIntegrationTest;
import models.Contribution;
import models.ContributionEvent;
import models.User;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.junit.ClassRule;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import update.ActorRule;

import java.io.File;

import static java.util.concurrent.TimeUnit.MINUTES;
import static models.ContributionEventType.CONTRIBUTION_GENERATED;
import static models.ContributionEventType.WEBREV_GENERATED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WebrevGeneratorST extends AbstractPersistenceIntegrationTest {

    @ClassRule
    public static ActorRule actorRule = new ActorRule();

    private static final String DONT_CARE = "";

    @Test
    public void generatesWebrev() throws Exception {
        User adoptopenjdk = User.findOrCreate("adoptopenjdk", "adoptopenjdk");
        Contribution contribution = new Contribution("jdk8-jdk", "1", DONT_CARE, DONT_CARE, adoptopenjdk, DateTime.now(), DateTime.now(), "default");
        File webrevLocation = contribution.webrevLocation();

        ContributionEvent message = new ContributionEvent(CONTRIBUTION_GENERATED);
        message.contribution = contribution;

        try {
            EventStream eventStream = actorRule.eventStream();
            ActorRef actor = actorRule.actorOf(WebrevGenerator.class);
            eventStream.subscribe(actor, ContributionEvent.class);
            eventStream.publish(message);

            Future<ContributionEvent> response = actorRule.expectMsg(ContributionEvent.class, MINUTES.toMillis(5));

            ContributionEvent reply = Await.result(response, Duration.Inf());
            assertEquals(WEBREV_GENERATED, reply.contributionEventType);

            assertTrue(webrevLocation.exists());
            assertTrue(webrevLocation.isDirectory());
            assertTrue(webrevLocation.list().length > 1);
        } finally {
            FileUtils.deleteDirectory(webrevLocation);
        }
    }

}
