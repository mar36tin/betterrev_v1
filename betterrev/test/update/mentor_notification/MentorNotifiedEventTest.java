package update.mentor_notification;

import akka.actor.ActorRef;
import com.icegreen.greenmail.util.GreenMail;
import models.Contribution;
import models.ContributionEvent;
import models.ContributionEventType;
import models.ContributionTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import update.pullrequest.AbstractPullRequestImporterTest;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * MentorNotifiedEventTest
 */
public class MentorNotifiedEventTest extends AbstractPullRequestImporterTest {

    private GreenMail mailServer;

    @Before
    public void setUp() throws Exception {
        mailServer = new GreenMail();
        mailServer.start();
    }

    @After
    public void tearDown() throws Exception {
        mailServer.stop();
        mailServer = null;
    }

    public List<MimeMessage> getReceivedMessages() {
        return Arrays.asList(mailServer.getReceivedMessages());
    }

    @Test
    public void contributionGenerationNotifiesMentorByEmail() throws Exception {
        Contribution contribution = ContributionTest.createTestInstance();
        ContributionEvent contributionEvent = contribution.contributionEvents.get(0);
        messageNotificationActor(contributionEvent);

        Future<ContributionEvent> response = actorRule.expectMsg(ContributionEvent.class, SECONDS.toMillis(10));
        Await.result(response, Duration.Inf());

        List<MimeMessage> receivedMessages = getReceivedMessages();
        checkEmail(contribution, receivedMessages);
    }

    private void checkEmail(Contribution contribution, List<MimeMessage> receivedMessages) throws MessagingException, IOException {
        assertEquals(1, receivedMessages.size());
        MimeMessage message = receivedMessages.get(0);
        assertEquals("RFR: " + contribution.name, message.getSubject());

        checkEmailContent(contribution, message);
    }

    private void checkEmailContent(Contribution contribution, MimeMessage message) throws MessagingException, IOException {
        assertTrue(message.getContentType().contains("text"));
        String body = (String) message.getContent();
        assertTrue(body.contains(contribution.description));
        assertTrue(body.contains(contribution.pullRequestUrl()));
        assertTrue(body.contains(contribution.requester.openjdkUsername));
    }

    @Test
    public void irrelevantEventsDontNotifyMentor() throws Exception {
        ContributionEvent contributionEvent = new ContributionEvent(ContributionEventType.TERMINATED);
        Future<ContributionEvent> response = messageNotificationActor(contributionEvent);

        ContributionEvent reply = Await.result(response, Duration.Inf());
        assertEquals(ContributionEventType.TERMINATED, reply.contributionEventType);

        actorRule.expectNoMsgWithinTimeout(ContributionEvent.class, 200);
    }

    private Future<ContributionEvent> messageNotificationActor(ContributionEvent contributionEvent) {
        ActorRef mentorNotificationActor = actorRule.actorOf(MentorNotificationActor.class);
        eventStream.subscribe(mentorNotificationActor, ContributionEvent.class);
        Future<ContributionEvent> first = actorRule.expectMsg(ContributionEvent.class, SECONDS.toMillis(10));
        eventStream.publish(contributionEvent);
        return first;
    }

}
